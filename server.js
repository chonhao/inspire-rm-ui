var http = require('http');
var fs = require('fs');
const { exec } = require('child_process');
var url = require('url');

const PORT_NUMBER = 8081;
const HOSTNAME = "127.0.0.1";

http.createServer(function (req, res) {
    var queries = url.parse(req.url, true).query;
    console.log(queries.map);
    fs.writeFile("input.txt", queries.map, function(err){
        if(err) console.log(err);
        
        exec('stage1.exe', (err, stdout, stderr) => {
            if(err) console.log(err);
            fs.readFile('output.txt', function(err, data) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                res.setHeader('Access-Control-Request-Method', '*');
                res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
                res.setHeader('Access-Control-Allow-Headers', '*');
                res.statusCode = 200;
                res.write(data);
                res.end();
            });
        });
    });
    
}).listen(PORT_NUMBER, HOSTNAME, ()=>{
    console.log("server.js running at port "+PORT_NUMBER);
});

