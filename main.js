var BOARD_WIDTH = 8;
var BOARD_HEIGHT = 8;

const ENABLE_NODE_INTEGRATION = true;

const TOOL_BLUE = 1;
const TOOL_RED = 2;
const TOOL_GREEN = 3;
const TOOL_YELLOW = 4;
const TOOL_DELETE = 0;
const COLOR_RED = "#FF3B6B";
const COLOR_BLUE = "#473DE8";
const COLOR_GREEN = "#2EFFC6";
const COLOR_YELLOW = "#E8A22A";
const COLOR_EMPTY = "#EEEEEE";

var current_tool = 0;

var _x_button;
var _red_button;
var _blue_button;
var _green_button;
var _yellow_button;

var _main_board;

var _textarea;
var _generate_button;
var _load_button;

var board_value = new Array(BOARD_HEIGHT);
for(var i=0;i<BOARD_HEIGHT;i++){
    board_value[i] = new Array(BOARD_WIDTH);
    for(var j=0;j<BOARD_WIDTH;j++){
        board_value[i][j] = 0;
    }
}

window.onload = function(){
    _x_button = document.getElementById("x_button");
    _red_button = document.getElementById("red_button");
    _blue_button = document.getElementById("blue_button");
    _green_button = document.getElementById("green_button");
    _yellow_button = document.getElementById("yellow_button");

    _x_button.onclick = function(){ toolSelected(TOOL_DELETE, this); };
    _red_button.onclick = function(){ toolSelected(TOOL_RED, this); };
    _blue_button.onclick = function(){ toolSelected(TOOL_BLUE, this); };
    _green_button.onclick = function(){ toolSelected(TOOL_GREEN, this); };
    _yellow_button.onclick = function(){ toolSelected(TOOL_YELLOW, this); };

    //------------------------------------------------------------------------
    
    _main_board = document.getElementById('main_board');

    //------------------------------------------------------------------------

    _textarea = document.getElementById('textarea');
    _generate_button = document.getElementById("generate_button");
    _load_button = document.getElementById("load_button");
    
    _generate_button.onclick = function(){
        var generated = generateText();
        _textarea.innerHTML = generated;
        if(ENABLE_NODE_INTEGRATION){
            $.ajax({
                type: 'GET',
                url: "http://127.0.0.1:8081",
                data: {map: generated},
            })
            .done(function(data){
                console.log(data);
                updateTableFromLoad(loadText(data));
            })
            .fail(function(jqXHR, textStatus, error){
                console.log(error);
            });
        }
    };
    _load_button.onclick = function(){
        updateTableFromLoad(loadText(_textarea.value));

    }

    

    

    drawTable();
};

function drawTable(){
    var content = "";
    for(var i=0;i<BOARD_HEIGHT;i++){
        content+="<tr>";
        for(var j=0;j<BOARD_WIDTH;j++){
            content+="<td></td>";
        }
        content+="</tr>";
    }
    _main_board.innerHTML = content;
    for(var i = 0;i<BOARD_HEIGHT;i++){
        for(var j=0;j<BOARD_WIDTH;j++){
            _main_board.rows[i].cells[j].onclick = function(){
                var row = this.parentNode.rowIndex;
                var col = this.cellIndex;
                board_value[row][col] = current_tool;
                switch(current_tool){
                    case TOOL_DELETE:
                        this.style.background = COLOR_EMPTY;
                        break;
                    case TOOL_RED:
                        this.style.background = COLOR_RED;
                        break;
                    case TOOL_BLUE:
                        this.style.background = COLOR_BLUE;
                        break;
                    case TOOL_GREEN:
                        this.style.background = COLOR_GREEN;
                        break;
                    case TOOL_YELLOW:
                        this.style.background = COLOR_YELLOW;
                        break;
                }
            };
        }
    }
}

function toolSelected(toolName, element){
    _x_button.classList.remove("button_selected");
    _red_button.classList.remove("button_selected");
    _blue_button.classList.remove("button_selected");
    _green_button.classList.remove("button_selected");
    _yellow_button.classList.remove("button_selected");
    current_tool = toolName;
    switch(toolName){
        case TOOL_DELETE:
            element.classList.add("button_selected");
            break;
        case TOOL_RED:
            element.classList.add("button_selected");
            break;
        case TOOL_BLUE:
            element.classList.add("button_selected");
            break;
        case TOOL_GREEN:
            element.classList.add("button_selected");
            break;
        case TOOL_YELLOW:
            element.classList.add("button_selected");
            break;
    }
}

function generateText(){
    var content = "";
    content += BOARD_WIDTH+" "+BOARD_HEIGHT+"\n";
    for(var i=0;i<BOARD_HEIGHT;i++){
        for(var j=0;j<BOARD_WIDTH;j++){
            content += board_value[i][j]+" ";
        }
        content+="\n";
    }
    return content;
}

function loadText(text){
    var temp = (' '+text).slice(1);
    var result_array = [];
    var rows = text.split('\n');
    for(var i=0;i<rows.length;i++){
        var cells = rows[i].split(' ');
        temp_array = [];
        for(var j=0; j<cells.length;j++){
            if(cells[j]>'0' && cells[j]<'9')
                temp_array.push(parseInt(cells[j]));
        }
        if(temp_array.length>0)
            result_array.push(temp_array);
    }
    console.log(result_array)
    return result_array;
}

function updateTableFromLoad(result_array){
    for(var i=0;i<result_array.length;i++){
        for(var j=0; j<result_array[i].length; j++){
            var temp_color;
            switch(result_array[i][j]){
                case 1:
                    temp_color = COLOR_BLUE;
                    break;
                case 2:
                    temp_color = COLOR_RED;
                    break;
                case 3:
                    temp_color = COLOR_GREEN;
                    break;
                case 4:
                    temp_color = COLOR_YELLOW;
                    break;
                default:
                    console.log("###exception detected");
                    break;
            }
            _main_board.rows[i].cells[j].style.background = temp_color;
            _main_board.rows[i].cells[j].style.filter = "opacity(40%)";

            if(board_value[i][j] != 0){
                _main_board.rows[i].cells[j].style.filter = "";
            }
        }
    }
}